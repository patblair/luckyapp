//
//  Guesser.swift
//  luckyApp
//
//  Created by Blair,Patrick T on 2/26/19.
//  Copyright © 2019 Blair,Patrick T. All rights reserved.
//

import Foundation

struct Guess {
    var correctAnswer:Int
    var numAttemptsRequired:Int
}
class Guesser {
    static let shared = Guesser()
    
    private var correctAnswer:Int
    private var _numAttempts:Int
    private var guesses:[Guess] = []
    public var numAttempts:Int {
        return _numAttempts
    }
    
    private init() {
        self.correctAnswer = Int.random(in: 1...10)
        self._numAttempts = 0
    }
    
    enum Result:String {
        case tooLow = "Too Low"
        case tooHigh = "Too High"
        case correct = "Correct"
    }
    
    func createNewProblem() {
        correctAnswer = Int.random(in: 1 ... 10)
        _numAttempts = 0
    }
    
    func amIRight(guess:Int) -> Guesser.Result {
        _numAttempts += 1
        if guess != correctAnswer {
            if guess > correctAnswer {
                return Result.tooHigh
            }
            return Result.tooLow
        }
        guesses.append(Guess(correctAnswer: correctAnswer, numAttemptsRequired: _numAttempts))
        return Result.correct
    }
    
    func guess(index:Int) -> Guess? {
        if index < 0 || index >= guesses.count {
            return nil
        }
        return guesses[index]
    }
    
    func numGuesses() -> Int {
        return guesses.count
    }
    
    func clearStatistics() {
        guesses = []
    }
    
    func minimumNumAttempts() -> Int {
        var min:Int = -1
        for guess in guesses {
            if guess.numAttemptsRequired < min || min < 0 {
                min = guess.numAttemptsRequired
            }
        }
        return min
    }
    
    func maximumNumAttempts() -> Int {
        var max:Int = -1
        for guess in guesses {
            if guess.numAttemptsRequired > max || max < 0 {
                max = guess.numAttemptsRequired
            }
        }
        return max
    }
    
    func meanNumAttempts() -> Double {
        if guesses.count == 0 {
            return 0
        }
        var sum:Double = 0
        for guess in guesses {
            sum += Double(guess.numAttemptsRequired)
        }
        return sum / Double(guesses.count)
    }
    
    func stdDevOfAttempts() -> Double {
        if guesses.count == 0 {
            return 0
        }
        let mean:Double = meanNumAttempts()
        var sum:Double = 0
        var attempts:Double
        for guess in guesses {
            attempts = Double(guess.numAttemptsRequired) - mean
            sum += attempts * attempts
        }
        return sqrt(sum / Double(guesses.count))
    }
}
