//
//  StatisticsVC.swift
//  luckyApp
//
//  Created by Blair,Patrick T on 2/27/19.
//  Copyright © 2019 Blair,Patrick T. All rights reserved.
//

import UIKit

class StatisticsVC: UIViewController {
    
    @IBOutlet weak var minLB: UILabel!
    @IBOutlet weak var maxLB: UILabel!
    @IBOutlet weak var meanLB: UILabel!
    @IBOutlet weak var stdDevLB: UILabel!
    var mean:Double = 0
    var stdD:Double = 0
    
    @IBAction func clearStats(_ sender: Any) {
        Guesser.shared.clearStatistics()
        minLB.text = ""
        maxLB.text = ""
        meanLB.text = ""
        stdDevLB.text = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mean = Guesser.shared.meanNumAttempts()
        stdD = Guesser.shared.stdDevOfAttempts()
        minLB.text = String(Guesser.shared.minimumNumAttempts())
        maxLB.text = String(Guesser.shared.maximumNumAttempts())
        meanLB.text = String(format: "%.2f", mean)
        stdDevLB.text = String(format: "%.2f", stdD)
    }
}
