//
//  FirstViewController.swift
//  luckyApp
//
//  Created by Blair,Patrick T on 2/21/19.
//  Copyright © 2019 Blair,Patrick T. All rights reserved.
//

import UIKit

class MainVC: UIViewController {

    @IBOutlet weak var guessTF: UITextField!
    @IBOutlet weak var closeLB: UILabel!
    var guesser = Guesser.shared
    
    @IBAction func checkAnswerBTN(_ sender: Any) {
        let guess = Int(guessTF.text!) ?? -1
        if guess == -1 {
            closeLB.text = "Invalid input"
        } else {
            closeLB.text = String(Substring(guesser.amIRight(guess: guess).rawValue))
            if closeLB.text! == "Correct" {
                displayMessage()
                guesser.createNewProblem()
            }
        }
    }
    
    @IBAction func newProblemBTN(_ sender: Any) {
        guesser.createNewProblem()
    }
    
    func displayMessage() {
        let alert = UIAlertController(title: "Well done", message: "You got it in \(Guesser.shared.numAttempts) tries", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
}
