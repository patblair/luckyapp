//
//  SecondViewController.swift
//  luckyApp
//
//  Created by Blair,Patrick T on 2/21/19.
//  Copyright © 2019 Blair,Patrick T. All rights reserved.
//

import UIKit

class GuessHistoryVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    //let TouristSites:[String] = ["Washington", "Colorado", "California", "New York", "TEST"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return Guesser.shared.numGuesses()
        } else {
            return -1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GuessView")
        let answer:Int = (Guesser.shared.guess(index: indexPath.row)?.correctAnswer) ?? 0
        let attempts:Int = (Guesser.shared.guess(index: indexPath.row)?.numAttemptsRequired) ?? 0
        if answer > 0 {
            cell!.textLabel?.text = "Answer: \(answer) ~ Attempts: \(attempts)"
        }
        //cell.textLabel?.text = TouristSites[indexPath.row]
        return cell!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
}
